FROM golang:1.18-alpine as environment
RUN apk add --no-cache git openssh-client ca-certificates
WORKDIR /go/src
ENV GO111MODULE=on

FROM environment AS builder
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod=vendor -o /go/bin/mason ./mason

FROM golang:1.18-alpine as release
WORKDIR /go
COPY --from=builder /go/bin/mason /go/bin/mason