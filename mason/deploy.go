package main

import (
	"context"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/deploy"
)

func createDeployCommand(mason *masonGo.Mason) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "deploy",
		Short: "deploy existing artifacts to groups on the Mason Platform",
		Long:  "This command will deploy artifacts to a specific group. See subcommands for details on deploying each artifact type.",
	}

	cmd.AddCommand(
		deployOtaCommand(mason),
		deployAPKCommand(mason),
		deployConfigCommand(mason))
	return cmd
}

func deployAPKCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group, name string
	var push bool

	var cmd = &cobra.Command{
		Use:     "apk",
		Short:   "deploy an APK to a group",
		Example: "mason deploy apk --name awesome-app-1 --version 3 --group group2",
		Long:    "This command will deploy a registered APK to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying APK: name: %s, version: %s, group: %s", name, version, group))
			err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    name,
				Group:   group,
				Version: version,
				Type:    "apk",
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("apk deployed")
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push)
	setNameParameter(cmd, &name)
	return cmd
}

func deployConfigCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group, name string
	var push bool

	var cmd = &cobra.Command{
		Use:     "config",
		Short:   "deploy a config to a group",
		Example: "mason deploy config --name app-config-1 --version 1 --group group3",
		Long:    "This command will deploy a registered config to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying OTA: name: %s, version: %s, group: %s", name, version, group))
			err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    name,
				Group:   group,
				Version: version,
				Type:    "config",
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("config deployed")
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push)
	setNameParameter(cmd, &name)
	return cmd
}

func deployOtaCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group string
	var push bool

	var cmd = &cobra.Command{
		Use:     "ota",
		Short:   "deploy an ota to a group",
		Example: "mason deploy ota --version 2.13.0 --group group3",
		Long:    "This command will deploy an OTA upgrade to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying OTA: version: %s, group: %s", version, group))
			err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    "mason-os",
				Group:   group,
				Version: version,
				Type:    "ota",
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("ota deployed")
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push)
	return cmd
}

func setStandardDeploymentFlags(cmd *cobra.Command, version, group *string, push *bool) {
	cmd.Flags().StringVarP(version, "version", "v", "", "Version of the artifact to deploy.")
	cmd.Flags().StringVarP(group, "group", "g", "", "Group to deploy the artifact to.")
	cmd.Flags().BoolVarP(push, "push", "p", false, "Push immediately to devices.")
	cmd.MarkFlagRequired("version")
	cmd.MarkFlagRequired("group")
}
func setNameParameter(cmd *cobra.Command, name *string) {
	cmd.Flags().StringVarP(name, "name", "n", "", "Unique name of the apk to deploy.")
	cmd.MarkFlagRequired("name")
}
