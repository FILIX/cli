package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/project"
)

func createProjectCmd(mason *masonGo.Mason) *cobra.Command {
	var name, description string
	var apiLevel APILevelFlag
	var deviceFamily DeviceFamilyFlag

	var cmd = &cobra.Command{
		Use:   "project",
		Short: "create project on the Mason Platform",
		Long: `This command will create project for the specified Device Family
			and API Level if they are valid.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("creating project")
			project, err := mason.CreateProject(context.Background(),
				project.CreateProjectOpts{
					Name:         &name,
					APILevel:     &apiLevel.APILevel,
					Description:  &description,
					DeviceFamily: &deviceFamily.DeviceFamily,
				})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("created project ", prettyPrint(project))
		},
	}
	cmd.Flags().StringVarP(&name, "name", "n", "", "name of the project")
	cmd.Flags().StringVarP(&description, "description", "", "", "description of the project")
	cmd.Flags().Var(&deviceFamily, "deviceFamily",
		`deviceFamily of the project, valid values are [D03, F07, D450, D450A, D450B, C210, F210, F450, G430, G450, I3399A, A4100, D215]`)
	cmd.Flags().Var(&apiLevel, "apiLevel", "apiLevel of the project, valid values are [23, 25, 27, 30]")
	cmd.MarkFlagRequired("name")
	cmd.MarkFlagRequired("description")
	cmd.MarkFlagRequired("deviceFamily")
	cmd.MarkFlagRequired("apiLevel")
	return cmd
}

func getProjectCmd(mason *masonGo.Mason) *cobra.Command {
	var name string

	var cmd = &cobra.Command{
		Use:   "project",
		Short: "get project on the Mason Platform",
		Long:  "This command will get project given the project identifier/name.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("getting project")
			project, err := mason.GetProject(context.Background(), name)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("get project ", prettyPrint(project))
		},
	}
	cmd.Flags().StringVarP(&name, "name", "n", "", "name of the project")
	cmd.MarkFlagRequired("name")
	return cmd
}

func prettyPrint(v interface{}) string {
	bytes, _ := json.MarshalIndent(v, "", "    ")
	return string(bytes)
}

type APILevelFlag struct {
	project.APILevel
}

func (a *APILevelFlag) String() string {
	return strconv.Itoa(int((*a).APILevel))
}

func (a *APILevelFlag) Set(s string) error {
	parsed, err := strconv.ParseInt(s, 0, 64)
	if err != nil {
		return err
	}
	(*a).APILevel = project.APILevel(parsed)
	if a.APILevel.IsValid() {
		return nil
	}
	return fmt.Errorf("invalid API Level (%s)", s)
}

func (a APILevelFlag) Type() string {
	return "API Level"
}

type DeviceFamilyFlag struct {
	project.DeviceFamily
}

func (d *DeviceFamilyFlag) String() string {
	return string(d.DeviceFamily)
}

func (d *DeviceFamilyFlag) Set(s string) error {
	(*d).DeviceFamily = project.DeviceFamily(s)
	if d.DeviceFamily.IsValid() {
		return nil
	}
	return fmt.Errorf("invalid Device Family (%s)", s)
}

func (a DeviceFamilyFlag) Type() string {
	return "Device Family"
}
