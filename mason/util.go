package main

import (
	"encoding/json"
	"fmt"
)

func DumpJSON(i interface{}) {
	s, _ := json.MarshalIndent(i, "", "  ")
	fmt.Println(string(s))
}
