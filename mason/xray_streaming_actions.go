package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"os"
)

func adbProxyCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var port string

	var cmd = &cobra.Command{
		Use:   "adbproxy",
		Short: "initiate adb proxy with a remote device",
		Long:  "This command will create a connection with a remote device allowing you to connect a locally using adb.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray adbproxy")

			xrayClient, err := mason.NewLocalProxyStreamingXrayClient(context.Background(), port, *deviceId, "adb")
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			err = xrayClient.HandleClientWebRTC(context.Background())
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
		},
	}

	cmd.Flags().StringVarP(&port, "port", "p", "5558", "localhost port to listen on")
	return cmd
}

func logcatCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var buffer string
	var level string
	var pid string
	var tags []string

	var cmd = &cobra.Command{
		Use:   "logcat",
		Short: "stream logs from a remote device",
		Long:  "This command will create a connection with a remote device allowing you to stream logs from it.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("xray logcat")

			// Add optional parameters. These will get url encoded later
			queryString := ""
			if buffer != "" {
				log.Debug("using buffer: ", buffer)
				queryString = appendQueryString(queryString, "buffer", buffer)
			}
			if level != "" {
				log.Debug("using level: ", level)
				queryString = appendQueryString(queryString, "level", level)
			}
			if pid != "" {
				log.Debug("using PID: ", pid)
				queryString = appendQueryString(queryString, "pid", pid)
			}
			for _, tag := range tags {
				queryString = appendQueryString(queryString, "tags", tag)
			}
			if len(tags) > 0 {
				log.Debug("using tags: ", tags)
			}

			xrayClient, err := mason.NewStreamingXrayClient(context.Background(), *deviceId, "logcat", queryString)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			err = xrayClient.HandleClientWebRTC(context.Background())
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
		},
	}

	cmd.Flags().StringVarP(&buffer, "buffer", "b", "", "log buffer on remote device to describe (main, system, crash, etc.)")
	cmd.Flags().StringVarP(&level, "level", "l", "", "log level to filter for. (verbose, debug, info, warn, error)")
	cmd.Flags().StringVarP(&pid, "pid", "p", "", "pid of process to filter logs for")
	cmd.Flags().StringArrayVarP(&tags, "tags", "t", make([]string, 0), "list of tags to filter device logs for (usage: -t Tag1 -t Tag2 etc.)")
	return cmd
}

func appendQueryString(originalQuery, name, value string) string {
	queryString := ""
	if originalQuery == "" && name != "" && value != "" {
		queryString = "?"
	}
	if name != "" && value != "" {
		queryString = queryString + fmt.Sprintf("&%s=%s", name, value)
	}
	return queryString
}
