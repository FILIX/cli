package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/xray"
	"io/ioutil"
	"os"
	"time"
)

func listFilesCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "list-files",
		Short: "list the given file path on a remote device",
		Long:  "This command will take the supplied filepath and list the directory or file on the remote device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray list-files")
			result, err := mason.ListFiles(context.Background(), *deviceId, path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			fmt.Println("Found ", len(result.Files), "Files:")
			for _, file := range result.Files {
				fmt.Println(file)
			}
			fmt.Println("\nFound ", len(result.Directories), "Directories:")
			for _, file := range result.Directories {
				fmt.Println(file)
			}
		},
	}

	cmd.Flags().StringVarP(&path, "path", "p", "", "file path on remote device")
	cmd.MarkFlagRequired("path")
	return cmd
}

func getFileCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var path string
	var outputPath string

	var cmd = &cobra.Command{
		Use:   "get-file",
		Short: "get the given file from the remote device",
		Long:  "This command will take the supplied filepath and return the file on the remote device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray get-file")

			result, err := mason.GetFile(context.Background(), *deviceId, path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			f, err := os.Create(outputPath)
			if err != nil {
				log.Error(err)
			}
			defer f.Close()

			_, err = f.Write(result)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Successfully wrote file to: ", outputPath)
		},
	}

	cmd.Flags().StringVarP(&path, "path", "p", "", "file path on remote device")
	cmd.Flags().StringVarP(&outputPath, "outputPath", "o", "", "file path to write the output to")
	cmd.MarkFlagRequired("path")
	cmd.MarkFlagRequired("outputPath")
	return cmd
}

func putFileCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var path string
	var filePath string

	var cmd = &cobra.Command{
		Use:   "put-file",
		Short: "put the given file path on the remote device",
		Long:  "This command will take the supplied filepath and put the given file on the remote device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray put-file")

			file, err := os.Open(filePath)
			if err != nil {
				log.Error(err)
				os.Exit(1)
			}
			fileContents, err := ioutil.ReadAll(file)
			if err != nil {
				log.Error(err)
				os.Exit(1)
			}
			fi, err := file.Stat()
			if err != nil {
				log.Error(err)
				os.Exit(1)
			}
			defer file.Close()

			err = mason.PutFile(context.Background(), *deviceId, path, fileContents, fi.Name())
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			fmt.Println("Successfully put file on remote device")
		},
	}

	cmd.Flags().StringVarP(&path, "path", "p", "", "file path on remote device")
	cmd.Flags().StringVarP(&filePath, "filePath", "f", "", "local path of file to put on remote device")
	cmd.MarkFlagRequired("path")
	cmd.MarkFlagRequired("filePath")
	return cmd
}

// Delete file
func deleteFileCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "delete-file",
		Short: "delete the given file from the remote device",
		Long:  "This command will take the supplied file path and delete the file on the remote device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray delete-file")

			err := mason.DeleteFile(context.Background(), *deviceId, path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			fmt.Println("Successfully deleted file")
		},
	}

	cmd.Flags().StringVarP(&path, "path", "p", "", "file path on remote device")
	cmd.MarkFlagRequired("path")
	return cmd
}

// Describe log buffer
func describeLogBuffersCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var buffer string

	var cmd = &cobra.Command{
		Use:   "describe-log-buffers",
		Short: "describe the log buffers available to read on the device",
		Long:  "This command will show more detailed information such as chattiest PIDs for the specified buffer(s) on the device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray describe-log-buffers")

			result, err := mason.DescribeLogBuffers(context.Background(), *deviceId, buffer)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			DumpJSON(result)
		},
	}

	cmd.Flags().StringVarP(&buffer, "buffer", "b", "main", "log buffer on remote device to describe")
	return cmd
}

// get logs
func getLogsCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var buffer string
	var level string
	var count int
	var pid int
	var startTime string
	var tags []string

	var cmd = &cobra.Command{
		Use:   "get-logs",
		Short: "get the current log buffer on the device",
		Long:  "This command will output the current log buffer on the device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray get-logs")

			params := xray.GetLogsParameters{
				Buffer: buffer,
				Level:  level,
			}

			if count > 0 {
				params.Count = count
			}
			if pid > 0 {
				params.Pid = pid
			}
			if startTime != "" {
				params.StartTime = startTime
			}
			if len(tags) > 0 {
				params.Tags = tags
			}

			result, err := mason.GetLogs(context.Background(), *deviceId, params)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			DumpJSON(result)
		},
	}

	cmd.Flags().StringVarP(&buffer, "buffer", "b", "", "log buffer on remote device to describe (main, system, crash, etc.)")
	cmd.Flags().StringVarP(&level, "level", "l", "", "log level to filter for. (verbose, debug, info, warn, error)")
	cmd.Flags().IntVarP(&count, "count", "c", -1, "number of log entries to retrieve")
	cmd.Flags().IntVarP(&pid, "pid", "p", -1, "pid of process to filter logs for")
	cmd.Flags().StringVarP(&startTime, "startTime", "s", "", "starting date time to filter logs for (in device's time) accepted formats: 'MM-dd HH:mm:ss.SSS' and 'yyyy-MM-dd HH:mm:ss.SSS'")
	cmd.Flags().StringArrayVarP(&tags, "tags", "t", make([]string, 0), "list of tags to filter device logs for (usage: -t Tag1 -t Tag2 etc.)")
	return cmd
}

// Install
func installApkCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var filePath string
	var reinstall bool
	var allowTestApk bool
	var allowVersionDowngrade bool
	var grantRuntimePermissions bool

	var cmd = &cobra.Command{
		Use:   "install-apk",
		Short: "install the supplied apk on the remote device",
		Long:  "This command will take in a local apk file and install it on the supplied device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray install-apk")

			err := mason.InstallApk(context.Background(), xray.InstallApkParameters{
				DeviceId:                *deviceId,
				Reinstall:               reinstall,
				AllowTestApk:            allowTestApk,
				AllowVersionDowngrade:   allowVersionDowngrade,
				GrantRuntimePermissions: grantRuntimePermissions,
				FilePath:                filePath,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			fmt.Println("Successfully installed apk")
		},
	}

	cmd.Flags().StringVarP(&filePath, "filePath", "f", "", "local path of file to put on remote device")
	cmd.Flags().BoolVarP(&reinstall, "reinstall", "r", false, "reinstall the apk if already present, keeping all of its data. Default: false")
	cmd.Flags().BoolVarP(&allowTestApk, "allowTestApk", "t", false, "allow the install of test apks. Default: false")
	cmd.Flags().BoolVarP(&allowVersionDowngrade, "allowVersionDowngrade", "v", false, "allow a version downgrade for the apk. Default: false")
	cmd.Flags().BoolVarP(&grantRuntimePermissions, "grantRuntimePermissions", "g", false, "grant all permissions listed in the app manifest. Default: false")
	cmd.MarkFlagRequired("filePath")
	return cmd
}

// Uninstall
func uninstallApkCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var apkName string

	var cmd = &cobra.Command{
		Use:   "uninstall-apk",
		Short: "uninstall the given apk on the remote device",
		Long:  "This command will take in an apk name and uninstall it on the supplied device.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray uninstall-apk")

			err := mason.UninstallApk(context.Background(), *deviceId, apkName)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			fmt.Println("Successfully uninstalled apk")
		},
	}

	cmd.Flags().StringVarP(&apkName, "apkName", "a", "", "name of the apk on the remote device to uninstall")
	cmd.MarkFlagRequired("apkName")
	return cmd
}

// get bug report
func getBugReportCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var outputPath string

	var cmd = &cobra.Command{
		Use:   "get-bug-report",
		Short: "generate and retrieve the bug report from the remote device",
		Long:  "This command will generate a bug report on the remote device and return it. Note: generating a bug report can take several minutes.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray get-bug-report")

			result, err := mason.GetBugReport(context.Background(), *deviceId)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			// If no path is specified, set it to a default file name
			if outputPath == "" {
				outputPath = fmt.Sprintf("./bugReport-%s-%s.zip", *deviceId, time.Now().Format(time.RFC3339))
			}
			f, err := os.Create(outputPath)
			if err != nil {
				log.Error(err)
			}
			defer f.Close()

			_, err = f.Write(result)
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	cmd.Flags().StringVarP(&outputPath, "outputPath", "o", "", "file path to write the output to")
	return cmd
}

// get screen capture
func getScreenCaptureCmd(mason *masonGo.Mason, deviceId *string) *cobra.Command {
	var outputPath string

	var cmd = &cobra.Command{
		Use:   "get-screen-capture",
		Short: "capture and return the current screen view on the remote device",
		Long:  "This command will capture the current screen view on the remote device and return it.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("X-Ray get-screen-capture")

			result, err := mason.GetScreenCapture(context.Background(), *deviceId)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			// If no path is specified, set it to a default file name
			if outputPath == "" {
				outputPath = fmt.Sprintf("./screenCapture-%s-%s.png", *deviceId, time.Now().Format(time.RFC3339))
			}

			f, err := os.Create(outputPath)
			if err != nil {
				log.Error(err)
			}
			defer f.Close()

			_, err = f.Write(result)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Successfully wrote screen capture to ", outputPath)
		},
	}

	cmd.Flags().StringVarP(&outputPath, "outputPath", "o", "", "file path to write the output to (.png file type)")
	return cmd
}
