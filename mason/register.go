package main

import (
	"context"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"io/ioutil"
	"os"
	"strings"
)

func registerAPKsCmd(mason *masonGo.Mason) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "apk",
		Short: "register an APK",
		Long:  "This command will register an APK with the Platform. It will use the android:versionCode, android:packageName for the APK version and name. These must be unique.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("registering apk")
			f, err := os.Open(path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			defer f.Close()

			if !strings.HasSuffix(path, ".apk") {
				log.Error(errors.New("provided file must be apk format"))
				os.Exit(1)
			}

			data, err := ioutil.ReadAll(f)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			log.Info(fmt.Sprintf("registering apk: %s", path))

			result, err := mason.RegisterAPK(context.Background(), f.Name(), data)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			DumpJSON(result)
			log.Info("apk registered")
		},
	}

	cmd.Flags().StringVarP(&path, "file", "f", "", "Relative path to an APK")
	cmd.MarkFlagRequired("file")
	return cmd
}

func registerBootanimationCmd(mason *masonGo.Mason) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "bootanimation",
		Short: "register a Bootanimation",
		Long:  "This command will register a Bootanimation with the Platform.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("registering bootanimation")
			f, err := os.Open(path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			defer f.Close()

			if !strings.HasSuffix(path, ".zip") {
				log.Error(errors.New("provided file must be zip format"))
				os.Exit(1)
			}

			data, err := ioutil.ReadAll(f)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			log.Info(fmt.Sprintf("registering bootanimation: %s, %s", f.Name(), path))

			result, err := mason.RegisterBootanimation(context.Background(), f.Name(), data)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			DumpJSON(result)
			log.Info("artifact registered")
		},
	}
	cmd.Flags().StringVarP(&path, "file", "f", "", "Relative path to a Bootanimation")
	cmd.MarkFlagRequired("file")
	return cmd
}

func registerSplashCmd(mason *masonGo.Mason) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "splash",
		Short: "register a Splash image",
		Long:  "This command will register a Splash image with the Platform.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("registering splash image")

			f, err := os.Open(path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			defer f.Close()

			if !strings.HasSuffix(path, ".png") {
				log.Error(errors.New("provided file must be png format"))
				os.Exit(1)
			}

			data, err := ioutil.ReadAll(f)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			log.Info(fmt.Sprintf("registering splash: %s, %s", f.Name(), path))

			result, err := mason.RegisterSplash(context.Background(), f.Name(), data)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			DumpJSON(result)
			log.Info("splash artifact registered")
		},
	}

	cmd.Flags().StringVarP(&path, "file", "f", "", "Relative path to a splash image")
	cmd.MarkFlagRequired("file")
	return cmd
}

func registerConfigCmd(mason *masonGo.Mason) *cobra.Command {
	var path string

	var cmd = &cobra.Command{
		Use:   "config",
		Short: "register Mason OS configs",
		Long:  "This command will register a Mason config with the Platform.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Debug("registering config")
			f, err := os.Open(path)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			defer f.Close()

			if !(strings.HasSuffix(path, ".yml") || strings.HasSuffix(path, ".yaml")) {
				log.Error(errors.New("provided file must be yaml/yml format"))
				os.Exit(1)
			}

			data, err := ioutil.ReadAll(f)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info(fmt.Sprintf("registering config: %s, %s", f.Name(), path))

			result, err := mason.RegisterConfig(context.Background(), f.Name(), data)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			DumpJSON(result)
			log.Info("config artifact registered")
		},
	}

	cmd.Flags().StringVarP(&path, "file", "f", "", "Relative path to a config yaml file.")
	cmd.MarkFlagRequired("file")
	return cmd
}
