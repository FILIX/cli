package main

import (
	log "github.com/sirupsen/logrus"
	"os"
)

const (
	DEBUG string = "DEBUG"
	INFO  string = "INFO"
)

func configureLogger() {
	formatter := &log.TextFormatter{
		FullTimestamp:          true,
		DisableLevelTruncation: true,
		PadLevelText:           true,
	}
	log.SetFormatter(formatter)

	switch os.Getenv("LEVEL") {
	case DEBUG:
		log.SetLevel(log.DebugLevel)
	case INFO:
		log.SetLevel(log.InfoLevel)
	default:
		return
	}
}
