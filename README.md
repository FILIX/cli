# Mason CLI

The Mason CLI provides command line tools to help you manage your artifacts, configurations, and devices in the [Mason Platform](https://bymason.com/).

- [Get the CLI](#get-the-cli)
  - [go install](#go-install)
  - [Download a binary](#download-a-binary)
  - [Build from source](#build-from-source)
  - [Docker container](#docker-container)
- [Usage](#usage)
- [Mason documentation](#documentation)

## Get the CLI

To use the CLI, you can either:

1. go install
2. Download a pre-built executable for your platform
3. Build the CLI
4. Use a docker container

### Go install

If you have [Golang](https://go.dev/dl/)  installed on your system, you can run the command:

```bash
> go install gitlab.com/MasonAmericaPublic/CLI.git/mason@latest
> mason -h
```

The cli will be added to your PATH as `mason`.

### Download a binary

There are prebuilt binaries for various platforms. The following binaries are prebuilt
1. linux
2. mac
3. mac-m1
4. windows

Run the following commands to download the latest version of the cli for your platform. Replace the value for `PLATFORM` with the appropriate option above.

```bash
> PLATFORM=linux
> VERSION=latest
> curl https://gitlab.com/api/v4/projects/38097768/packages/generic/cli-releases/$VERSION/mason-cli-$PLATFORM --output mason
> chmod +x mason
> ./mason -h
```

To install a specific version, navigate to the [releases](https://gitlab.com/MasonAmericaPublic/cli/-/releases) page to the available versions. Then replace the value for `VERSION` with the desired release.

### Build from source

Ensure you have [Golang](https://go.dev/dl/) installed on your machine. Then clone this repo and run the following command:

```bash
> go build -o mason ./mason
> ./mason -h
```

### Docker container

Visit the [container registry](https://gitlab.com/MasonAmericaPublic/cli/container_registry) for a full list of available images. Run the following command to pull and run the latest version.

```bash
> docker run registry.gitlab.com/MasonAmericaPublic/cli:latest mason -h
```

## Usage

ALl commands in the CLI are documented with help flags (-h). To see all available commands, run:

```bash
> mason -h
```

The help flag can be passed to any subcommand as well:

```bash
> mason register -h
```

## Documentation

All Mason Platform documentation can be found at [the Mason docs site](https://docs.bymason.com/).